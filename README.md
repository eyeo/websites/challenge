# DEPRECATED eyeo website developer challenge

This challenge is no longer maintained or in use.

## Wireframes

- [Small](/wireframes/small.png) (320x480 / iphone SE)
- [Medium](/wireframes/medium.png) (768x1024 / iPad portrait)
- [Large](/wireframes/large.png) (1024x768 / iPad landscape)

## Limitations

- Use vanilla HTML, CSS, and JavaScript
    - No libraries, frameworks, components, etc

## Browser support

- IE9+

Don't know how to test IE in MacOS or GNU/Linux? See [modern.ie](https://modern.ie).

## Specifications

### Fixed navbar

Containing:

1. Menu Toggle
2. Menu
   1. Link to Section 1
   2. Link to Section 2

The toggle button should toggle a menu containing the two links on small screens.

### Section 1

#### Image

Place an image to the right of content below on large screens.

#### Heading

`Lorem ipsum dolor sit amet, consectetur adipiscing elit`

Break at the comma when it makes sense.

#### Paragraph

`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.`

#### Button

`Lorem ipsum dolor sit amet`

### Section 2

Place two components made up of the following content side-by-side on large screens and stacked on small screens.

#### Image

Place an image to the left of the content below on tablets.

#### Heading

`Lorem ipsum dolor sit amet, consectetur adipiscing elit`

Break at the comma when it makes sense.

#### Paragraph

`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.`

## Submission

1. Host your submission using Gitlab pages
    - Don't mention anything identifying in your repository (to avoid copycats)
2. Send the URL to your submission to eyeo

## Bonus

The challenge above was designed to be completable within one remote working day. If you ( were able to / think you can ) complete it in less than one remote working day, consider taking on one or more of the following bonus challenges:

1. Support both left-to-right and right-to-left directions
2. Make it fully accessible according to WCAG 2.0 guidelines
3. Wake in "no-js first" (No JavaScript is not "no-js first")

**Important**:

1. The limitations above apply to the bonus challenges too
2. Please state which bonus challenges you have attempted; if any
3. A submission without bonuses is better than a submission with incomplete or poorly implemented bonuses - all else being equal.
